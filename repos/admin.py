from django.contrib import admin
from .models import Repository, Report, Access


# Register your models here.

class InlineReportAdmin(admin.StackedInline):
    model = Report

class InlineAccessAdmin(admin.TabularInline):
    model = Access

class RepositoryAdmin(admin.ModelAdmin):
    inlines = [InlineAccessAdmin, InlineReportAdmin]

    def save_model(self, request, obj, form, change):
        if obj.creator == None:
            obj.creator = request.user
        obj.save()

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = []
        if not request.user.is_superuser:
            self.exclude.append('creator') #here!
        return super(RepositoryAdmin, self).get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        qs = super(RepositoryAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            return qs.filter(creator=request.user)
        return qs

admin.site.register(Repository, RepositoryAdmin)