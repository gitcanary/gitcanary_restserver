from rest_framework.response import Response

from .models import Repository, Report
from .serializers import RepositorySerializer, ReportSerializer, RepoReportSerializer
from rest_framework import viewsets, permissions
from rest_framework.decorators import action

class IsOwnerOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user.is_staff:
            return True

        return False

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `owner`.
        return obj.access.filter(user=request.user, edit=True).count() > 0

class ReportIsOwnerOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        if 'repo' in request.data:
            repo = Repository.objects.get(id=request.data['repo'])
            if repo.access.filter(user=request.user, edit=True).count() == 0:
                return False
        return True

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `owner`.
        return obj.repo.access.filter(user=request.user, edit=True).count() > 0

# ViewSets define the view behavior.
class RepositoryViewSet(viewsets.ModelViewSet):
    permission_classes = [IsOwnerOrReadOnly]
    queryset = Repository.objects.all()
    serializer_class = RepositorySerializer

    def get_queryset(self):
        return Repository.objects.filter(access__user=self.request.user)

    @action(detail=True)
    def reports(self, request, pk=None):
        repo = self.get_object()
        reports = repo.reports.all()
        serializer = RepoReportSerializer(reports, many=True)
        return Response(serializer.data)

class ReportViewSet(viewsets.ModelViewSet):
    permission_classes = [ReportIsOwnerOrReadOnly]

    queryset = Report.objects.all()
    serializer_class = ReportSerializer

    def get_queryset(self):
        return Report.objects.filter(repo__access__user=self.request.user)