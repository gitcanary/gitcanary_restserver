from .models import Repository, Report, Access
from rest_framework import serializers
from django.contrib.auth.models import User

class ReportShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = ('id', 'date', 'type')

class RepoAccessSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source='user.username', read_only=True)
    class Meta:
        model = Access
        fields = ('user', 'edit')

class RepositorySerializer(serializers.ModelSerializer):
    reports = ReportShortSerializer(many=True, read_only=True)
    access = RepoAccessSerializer(many=True, read_only=True)
    committers = serializers.JSONField()
    scanner_config = serializers.JSONField()

    class Meta:
        model = Repository
        fields = (
            'id',
            'name',
            'url',
            'committers',
            'reports',
            'scanner_config',
            'access'
        )

class ReportSerializer(serializers.ModelSerializer):
    json = serializers.JSONField()
    class Meta:
        model = Report
        fields = ('repo', 'date', 'type', 'json')

class RepoReportSerializer(serializers.ModelSerializer):
    json = serializers.JSONField()
    class Meta:
        model = Report
        fields = ('repo', 'date', 'type', 'json')