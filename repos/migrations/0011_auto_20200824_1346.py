# Generated by Django 3.1 on 2020-08-24 11:46

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('repos', '0010_auto_20200824_1311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repository',
            name='scanner_config',
            field=jsonfield.fields.JSONField(default={'ignore': {'authors': ['Frank van Viegen', 'Jan Jaap Sandee'], 'commits': [], 'files': []}, 'reports': ['base', 'gitblame']}),
        ),
    ]
