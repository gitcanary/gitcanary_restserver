# Generated by Django 3.1 on 2020-08-18 12:56

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('repos', '0006_repository_committers'),
    ]

    operations = [
        migrations.AddField(
            model_name='repository',
            name='commit_ignore_list',
            field=jsonfield.fields.JSONField(blank=True, null=True),
        ),
    ]
