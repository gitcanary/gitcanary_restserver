from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from jsonfield import JSONField

DEFAULT_CONFIG = {
    'reports': ['base', 'gitblame'],
    'ignore': {
        'files': [],
        'authors': ['Frank van Viegen', 'Jan Jaap Sandee'],
        'commits': []
    }
}

DEFAULT_COMMITTERS = {
    'Committer 1': [
        'GItname 1',
        'Gitname_1'
    ],
    'Commiter 2': [
        'Gitname2',
        'CoolGitname2'
    ]
}

class Repository(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField()
    committers = JSONField(blank=True, null=True, default=DEFAULT_COMMITTERS)
    scanner_config = JSONField(default=DEFAULT_CONFIG)
    creator = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, )
    def __str__(self):
        return u'%s' % self.name

class Report(models.Model):
    repo = models.ForeignKey(Repository, on_delete=models.CASCADE, related_name='reports')
    date = models.DateTimeField()
    type = models.CharField(max_length=50)
    json = JSONField(blank=True, null=True)


class Access(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    repo = models.ForeignKey(Repository, on_delete=models.CASCADE, related_name='access')
    edit = models.BooleanField(default=False, help_text="User can update information using the API.")